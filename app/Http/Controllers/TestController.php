<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        // Prefedine Data
        $User = new UserTest;
        $data = [];
        // Getting Data
        $user = $User->get();
        $data['data'] = $user;
        return view('index', $data);
    }

    public function store(Request $request)
    {
        //LOGIC HERE
        $data = [];
        $insert = [
            'name'      => $request->name,
            'parity'    => $request->parity
        ];        
        $id = UserTest::insertGetId($insert);

        $user = UserTest::find($id);
        if ($id % 2 == 0) {
            $user->parity = 'EVEN';
        } else {
            $user->parity = 'ODD';
        }
        
        $status = $user->save();
        
        return redirect()->route('index');
    }

}
